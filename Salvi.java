package gabrielss;
import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * Robot - a robot by Gabriel Silva
 */
public class Salvi extends AdvancedRobot
{

	int movementDirection = 1;
	long movedTime = 0;

	int spinTime = 0;
	boolean isSpining = false;

	//Creating a Enemy object to store data of current target
	Enemy enemyRobot = new Enemy();


	public void run() {
		setColors(Color.black,Color.black,Color.black); // body,gun,radar
		setScanColor(Color.red);

		// Set parts independent of each other
		setAdjustGunForRobotTurn(true);
		setAdjustRadarForGunTurn(true);
		setAdjustRadarForRobotTurn(true);

		setTurnRadarRightRadians(99999);
		do{
			movement();
			execute();
		}while(true);

	}

	// Generate a random number
	public int getRandomNumber(int min, int max){
		int random = min + (int)(Math.random()*max);
		return random;
	}

  // Create a main random movement
	public void movement(){
		if(checkTimeToMove()){
			int randomAngle = getRandomNumber(45,120);
			int randomAhead = getRandomNumber(100,500);

			setTurnRight(randomAngle * movementDirection);
			setAhead(randomAhead * movementDirection);
			velocity();

			movedTime = getTime();
			movementDirection = -movementDirection;
		}

	}

	// Check if it's time to move and don't cancel the current movement instantly
	public boolean checkTimeToMove(){
		int timeReactBasedOnEnemies = getEnemiesAmount() > 2 ? 10 : 5; 
		return ((getTime() - movedTime) > timeReactBasedOnEnemies || movedTime == 0 ) ? true : false;
	}

	// Get amount of enemies in the battlefield
	public int getEnemiesAmount(){
			scan();
			int enemyAmount = getOthers();
			return enemyAmount;
	}

	//Scape
	public void scape(double bearing){
		if(checkTimeToMove()){
			movementDirection = -movementDirection;
			setTurnRight(bearing - 90);
			setAhead(100 * movementDirection);
		}
	}
	
	// Aim on the enemy
	public void aim(Enemy enemy){
		double radarTurn = robocode.util.Utils.normalRelativeAngle(getHeadingRadians() + enemy.getBearing() - getRadarHeadingRadians());
		double gunTurn = robocode.util.Utils.normalRelativeAngle(getHeadingRadians() + enemy.getBearing() - getGunHeadingRadians());

		double extraTurn = Math.min(Math.atan(36.0 / enemy.getDistance()), Rules.RADAR_TURN_RATE_RADIANS);
	  radarTurn += (radarTurn < 0 ? -extraTurn : extraTurn);

		setTurnRadarRightRadians(radarTurn);
		setTurnGunRightRadians(gunTurn);
	}

	// Chase after an enemy
	public void chase(Enemy enemy){
		velocity();
		
		//Creating a circular movement around the target
		setTurnRight(enemy.getBearing() - 3.0);

		// Keeping a safe distance
		setAhead(enemy.getDistance() - 300);

	}

	// Create Random velocity to try dodge shoot calculations
	public void velocity(){
		setMaxVelocity(getRandomNumber(5,12));
	}

	public void onScannedRobot(ScannedRobotEvent currentEnemy) {
		enemyRobot.updateEnemy(currentEnemy);
		
		aim(enemyRobot);

		// Shoot only if it's nearest
		if(currentEnemy.getDistance() <= 600){
			fire(Math.min(400 / currentEnemy.getDistance(), 5));
		}
		chase(enemyRobot);

		setTurnRadarRightRadians(9999);

	}

	public void onHitByBullet(HitByBulletEvent hit) {
		scape(hit.getBearing());
	}
	

	public void onHitWall(HitWallEvent wall) {
		scape(wall.getBearing());
	}	

	public void onHitRobot(HitRobotEvent robot) {
		enemyRobot.updateEnemy(robot);
		aim(enemyRobot);
		fire(5);
	}

	public void onRobotDeath(RobotDeathEvent robot){
		if(enemyRobot.getName().contains(robot.getName())){
			enemyRobot.setExist(false);
		}
	}

}
