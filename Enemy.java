package gabrielss;
import robocode.*;

public class Enemy{
  private String name;
  private double energy;
  private double bearing;
  private double distance;
  private boolean exist;

  public Enemy(){
    this.name = "";
    this.energy = 0.0;
    this.bearing = 0.0;
    this.distance = 0.0;
    this.exist = false;
  }

  public void updateEnemy(ScannedRobotEvent enemy) {
    if(!this.exist || this.name.equals(enemy.getName()) || this.distance > enemy.getDistance()){
      this.name = enemy.getName();
      this.energy = enemy.getEnergy();
      this.bearing = enemy.getBearingRadians();
      this.distance = enemy.getDistance();
      this.exist = true;
    }
	}

  public void updateEnemy(HitRobotEvent enemy) {
      this.name = enemy.getName();
      this.energy = enemy.getEnergy();
      this.bearing = enemy.getBearingRadians();
      this.distance = 0.0;
      this.exist = true;
	}

	public String getName() {
		return name;
	}
	public double getEnergy() {
		return energy;
	}
  public double getBearing() {
		return bearing;
	}
	public double getDistance() {
		return distance;
	}
  public boolean getExist(){
    return exist;
  }
  public void setExist(boolean option){
    this.exist = option;
  }
}