# ROBOCODE - SOLUTIS TALENT SPRINT

## SALVI ROBOT

The code behind this robot was made thinking about how to create a singular behavior that can alternate it according with some rules or events;
It can chase after a robot or set a random movement if no one is on the radar;

### CHECKS

- Amount of enemy;
- Time to move again;

### MOVEMENT

- Time of the movement reaction is based on amount of enemies.
- Initial: Random movement based on time and amount of enemies in the battlefield;
- onHitRobot: Chase after it and fires;
- Velocity is randomly between 5 and 12 to try to avoid calculating enemies;

### TARGETING

- Initial: Turn radar around to scan enemies;
- onScannedRobot: Set radar and gun direction based on where a enemy was scanned and chase after him;

### SHOTING

- Fire power is based on distance of the enemy;

### WEAKNESSES

- Because it's movement is randomily, sometimes it can go right into a bullet;
- Can be miss some shots because always fire when it 'see' an enemy;
- When in chase mode can be more predictable;

### STRENGTHS

- Firepower balance based on distance;
- Targeting;
- High Damage on meeles;
- Scape Method;
- Chase;
- Try to dodge bullets on head-to-head;

### LEARNING

This project was something that made me do a lot of research and tests on the Robocode API giving me the opportunity to code in JAVA, wich I haven't done since College. It made me think about how to create a simple but effective algorithm that can last as long as possible on the battlefield.
And yeah, I had so much fun on it!
